package com.example.demo.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Alumne;
import com.example.demo.repositories.AlumneRepository;
import com.example.demo.services.AlumneService;

@RestController
public class Controllers 
{
	@Autowired
	AlumneRepository ar;
	
	@Autowired
	AlumneService service;
	
	@GetMapping(path="/")
	public String holaMon()
	{
		return "Hola mon";
	}
	
	@GetMapping(path = "/holaUser")
	public String holaUser(@RequestParam(required = false) String nomUser)
	{
		if(nomUser!=null)
			return "Hola "+nomUser;
		else
			return "Hola...";
	}
	
	
	@GetMapping(path="/nouAlumne")
	public String nouAlumne(@RequestParam String nom, @RequestParam boolean depresion, @RequestParam int edat, @RequestParam boolean fumon,@RequestParam double cafeina)
	{
		Alumne a = new Alumne();
		a.setNom(nom);
		a.setDepresion(depresion);
		a.setEdat(edat);
		a.setCafeina(cafeina);
		a.setIsFumon(fumon);
		ar.save(a);
		
		return "Todo ok!!! se ha insertado -> "+a.toString();
	}
	
	@GetMapping(path="/buscaRaul")
	public List<Alumne> buscaRaul()
	{
		return ar.findByNom("Raul");
	}
	
	@GetMapping(path="/buscaRaulDeJovenAViejo")
	public List<Alumne> buscaRaulDeJovenAViejo()
	{
		return ar.findByNomOrderByEdatAsc("Raul");
	}
	
	@GetMapping(path="/aniversari")
	public String aniversari(@RequestParam int id)
	{
		Optional<Alumne> a = ar.findById(id);
		if(a.isPresent())
		{
			service.aniversari(a.get());
			return "OLE";
		}			
		else
			return "FALLO PORQUE NO EXISTE ESA ID ¿ERES TONTI?";
	}
	
	
	
}
