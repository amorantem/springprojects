package com.example.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.models.Alumne;
import com.example.demo.repositories.AlumneRepository;

@Service("AlumneService")
public class AlumneService implements IAlumneService{

	@Autowired
	AlumneRepository ar;
	
	
	@Override
	public void aniversari(Alumne a) {
		a.setEdat(a.getEdat()+1);
		ar.save(a);		
	}

}
