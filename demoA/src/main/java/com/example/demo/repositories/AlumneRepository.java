package com.example.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.models.Alumne;

public interface AlumneRepository extends JpaRepository<Alumne, Integer>
{
	List<Alumne> findByNom(String nom);
	List<Alumne> findByNomOrderByEdatAsc(String nom);
	
}
