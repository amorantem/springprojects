package com.example.demo.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//Anotacio per a marcar la classe com a entitat
@Entity
//Anotacio per indicar el nom de la taula
@Table(name="alumne_spring")
public class Alumne 
{
	//@Id ens indica que aquest atribut es una clau primaria
	@Id
	//@GeneratedValue indica a Hibernate com ha de generar la ID
	@GeneratedValue(strategy = GenerationType.AUTO)
	//@Column ens permet modificar paràmetres de la columna
	@Column(name="id")
	private int id;
	private String nom;
	@Column(name="depresion")
	private Boolean depresion;
	@Column(name="edat")
	private int edat;
	@Column(name="isFumon")
	private Boolean isFumon;
	@Column(name="cafeina", columnDefinition = "double(10,2)")
	private double cafeina = 0.00;
	
	
	public Alumne() {
		super();
	}
	
	public Alumne(String nom, Boolean depresion, int edat, Boolean isFumon, double cafeina) {
		super();
		this.nom = nom;
		this.depresion = depresion;
		this.edat = edat;
		this.isFumon = isFumon;
		this.cafeina = cafeina;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Boolean getDepresion() {
		return depresion;
	}
	public void setDepresion(Boolean depresion) {
		this.depresion = depresion;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public Boolean getIsFumon() {
		return isFumon;
	}
	public void setIsFumon(Boolean isFumon) {
		this.isFumon = isFumon;
	}
	public double getCafeina() {
		return cafeina;
	}
	public void setCafeina(double cafeina) {
		this.cafeina = cafeina;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Alumne [id=" + id + ", nom=" + nom + ", depresion=" + depresion + ", edat=" + edat + ", isFumon="
				+ isFumon + ", cafeina=" + cafeina + "]";
	}
}
