package com.example.demo.services;

import java.util.List;

import com.example.demo.models.Joguina;

public interface IJoguinaService 
{
	Joguina findById(int id);
	List<Joguina> findByNom(String nom);
	void modificarDiversio(int novaDiv, Joguina j);
}
