package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.models.Joguina;
import com.example.demo.repositories.JoguinaRepository;

public class JoguinaService implements IJoguinaService {

	@Autowired
	JoguinaRepository jr;
	
	
	@Override
	public Joguina findById(int id) {
		// TODO Auto-generated method stub
		return jr.findById(id).orElseGet(null);
	}

	@Override
	public void modificarDiversio(int novaDiv, Joguina j) {
		j.setNivellDiversio(novaDiv);
		jr.save(j);
		
	}

	@Override
	public List<Joguina> findByNom(String nom) {
		// TODO Auto-generated method stub
		return jr.findByNom(nom);
	}

}
