package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Joguina;
import com.example.demo.repositories.JoguinaRepository;
import com.example.demo.services.JoguinaService;

@RestController
public class Controllers {
	
	@Autowired
	private JoguinaRepository jr;
	
	
	@Autowired
	private JoguinaService js;
	
	@GetMapping(path="/hola")
	public String sayHola()
	{
		return "hola";
	}
	
	@GetMapping(path="/addJoguina")
	public String addJoguina()
	{
		Joguina j = new Joguina();
		j.setDescripcio("és una joguina generada via url");
		j.setNom("urlrlrlrl");
		j.setNivellDiversio(20);
		jr.save(j);
		return "joguina generada";
		
	}
	
	
	@GetMapping(path="/addJoguinaName")
	public String addJoguinaByName(@RequestParam(required = false) String nom)
	{
		Joguina j = new Joguina();
		j.setDescripcio("és una joguina generada via url");
		j.setNom(nom);
		j.setNivellDiversio(20);
		jr.save(j);
		
		return "joguina generada";
		
	}
	
	
	@GetMapping(path="/getJoguinesByNom")
	public List<Joguina> getJoguinesByNom(@RequestParam String nomJoguina)
	{
		return jr.findByNom(nomJoguina);
	}
	
	@GetMapping(path="/byLvl")
	public List<Joguina> getJoguinesByLvl(@RequestParam int lvl)
	{
		return jr.findByNivellDiversioOrderByNomDesc(lvl);
	}
}
