package com.example.demo.models;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Joguina 
{
	@Id@GeneratedValue
	private int id;
	private String descripcio;
	private int nivellDiversio;
	private String nom;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcio() {
		return descripcio;
	}
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
	public int getNivellDiversio() {
		return nivellDiversio;
	}
	public void setNivellDiversio(int nivellDiversio) {
		this.nivellDiversio = nivellDiversio;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Joguina() {
		super();
	}
	
	
	
}
