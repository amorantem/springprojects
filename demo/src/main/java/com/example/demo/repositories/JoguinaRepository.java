package com.example.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.models.Joguina;

public interface JoguinaRepository extends JpaRepository<Joguina, Integer>{
	//esto es magia, fin
	List<Joguina> findByNom(String nom);
	List<Joguina> findByNivellDiversioOrderByNomDesc(int nivell);
}
