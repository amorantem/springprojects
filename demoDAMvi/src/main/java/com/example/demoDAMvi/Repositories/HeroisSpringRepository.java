package com.example.demoDAMvi.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demoDAMvi.Models.HeroisSpring;

public interface HeroisSpringRepository extends JpaRepository<HeroisSpring, Integer>
{
	List<HeroisSpring> findByNom(String nom);
	List<HeroisSpring> findByNomOrderByPoderDesc(String nom);
}
