package com.example.demoDAMvi.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demoDAMvi.Models.HeroisSpring;
import com.example.demoDAMvi.Repositories.HeroisSpringRepository;

@Service("HeroisSpringService")
public class HeroisSpringService implements IHeroisSpringService
{
	@Autowired
	HeroisSpringRepository ahs;
	
	
	@Override
	public List<HeroisSpring> findByNom(String nom) {
		return ahs.findByNom(nom);
	}

	@Override
	public List<HeroisSpring> findByNomOrderByPoderDesc(String nom) {
		return ahs.findByNomOrderByPoderDesc(nom);
	}

	@Override
	public String milloraHeroi(int id, double poderASumar) {
		HeroisSpring h = ahs.findById(id).orElse(null);
		if(h==null) return "Este heroi no esite";
		
		h.setPoder(h.getPoder()+poderASumar);
		ahs.save(h);
		
		return h.toString();
	}

}
