package com.example.demoDAMvi.Services;

import java.util.List;

import com.example.demoDAMvi.Models.HeroisSpring;

public interface IHeroisSpringService 
{
	List<HeroisSpring> findByNom(String nom);
	List<HeroisSpring> findByNomOrderByPoderDesc(String nom);
	String milloraHeroi(int id, double poderASumar);
}
