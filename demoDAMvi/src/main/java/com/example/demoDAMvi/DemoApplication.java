package com.example.demoDAMvi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoDAMvi.Models.HeroisSpring;
import com.example.demoDAMvi.Repositories.HeroisSpringRepository;

@SpringBootApplication
@ComponentScan({"com.example.demoDAMvi"})
@EntityScan("com.example.demoDAMvi")
@EnableJpaRepositories("com.example.demoDAMvi")
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}


